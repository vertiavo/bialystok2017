package com.acaisoft.openday.db;

import com.acaisoft.openday.schema.Book;
import com.acaisoft.openday.views.books.exceptions.BookBorrowException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class BookslistDB {
    private List<Book> booksList;

    public BookslistDB() {
        this.booksList = new ArrayList<>();
        this.booksList = BookslistDBMocks.getMocks();
    }

    public List<Book> getBooksList() {
        return this.booksList;
    }

    public Book borrowBook(Integer id) throws BookBorrowException {
        Book book = this.booksList.get(id);

        if (book.getQuantity() > 0)
            return book;
        else
            throw new BookBorrowException("There is not enough quantity.");
    }
}
