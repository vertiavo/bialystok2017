import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  notificationOptions = {
    position: ["top", "left"],
    timeOut: 5000,
    lastOnBottom: true,
    showProgressBar: true,
    pausOnHover: true,
    clickToClose: true
  };
}
