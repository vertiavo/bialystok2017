package com.acaisoft.openday.security.impl;


import com.acaisoft.openday.security.JwtUser;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtUserImpl extends  UserDetailsWrap implements JwtUser {
    private final String jwtKey;

    public JwtUserImpl(String jwtKey, UserDetails origin) {
        super(origin);
        this.jwtKey = jwtKey;
    }

    @Override
    public String token() throws Exception {
        Map<String, Object> claims = createClaims();
        return compact(claims, this.jwtKey);
    }


    private Map<String, Object> createClaims(){
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIMS_PARAM_NAMES.USER_NAME, this.getUsername());
        claims.put(CLAIMS_PARAM_NAMES.ROLES, getAuthorities().stream()
                                                .map(authority -> authority.getAuthority())
                                                .collect(Collectors.toList())
                                                .toArray(new String[0])
        );

        return claims;
    }


    private String compact(Map<String, Object> claims, String key){

        return Jwts.builder()
            .setClaims(claims)
            .signWith(SignatureAlgorithm.HS512, key)
            .compact();
    }
}
