package com.acaisoft.openday.views.books.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by vertiavo on 17.05.17.
 */

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BookBorrowException extends Exception {

    public BookBorrowException(String message) {
        super(message);
    }
}
