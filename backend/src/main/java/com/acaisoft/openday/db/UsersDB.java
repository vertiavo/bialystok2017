package com.acaisoft.openday.db;

import com.acaisoft.openday.schema.User;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UsersDB {
    private List<User> usersList;

    public UsersDB() {
        this.usersList = new ArrayList<>();
        this.mockUsers();
    }

    private void mockUsers() {
        this.usersList.addAll(Arrays.asList(
            new User()
                .setUsername("admin")
                .setPassword("admin")));
    }

    public Optional<User> getUser(String username) {
        return this.usersList.stream()
            .filter(user -> Objects.equals(user.getUsername(), username))
            .findFirst();
    }

    /*
        Registers a user in a database
     */
    public synchronized void registerUser(String username, String password) throws UsersDBException {
        Optional<User> foundUserOpt = this.getUser(username);

        if (foundUserOpt.isPresent()) {
            throw new UsersDBException("User with the name: " + username + " already exists");
        }

        this.usersList.add(new User()
            .setUsername(username)
            .setPassword(password));
    }

    /*
        Checks if there is a user with provided email and password
     */
    public boolean areCredentialsValid(String username, String password) {
        Optional<User> foundUserOpt = this.usersList.stream()
            .filter(user -> Objects.equals(user.getUsername(), username))
            .filter(user -> Objects.equals(user.getPassword(), password))
            .findFirst();

        return foundUserOpt.isPresent();
    }
}
