import { LibraryWIOpenDayPage } from './app.po';

describe('library-wiopen-day App', () => {
  let page: LibraryWIOpenDayPage;

  beforeEach(() => {
    page = new LibraryWIOpenDayPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
