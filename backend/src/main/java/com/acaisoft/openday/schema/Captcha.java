package com.acaisoft.openday.schema;

import com.acaisoft.openday.views.user.dto.CaptchaDto;

public class Captcha {
    private String formId;
    private String textValue;
    private boolean expired;

    public String getFormId() {
        return formId;
    }

    public Captcha setFormId(String formId) {
        this.formId = formId;
        return this;
    }

    public String getTextValue() {
        return textValue;
    }

    public Captcha setTextValue(String textValue) {
        this.textValue = textValue;
        return this;
    }

    public boolean isExpired() {
        return expired;
    }

    public Captcha setExpired(boolean expired) {
        this.expired = expired;
        return this;
    }

    public CaptchaDto toDto() {
        return new CaptchaDto()
            .setFormId(this.formId)
            .setTextValue(this.textValue);
    }
}
