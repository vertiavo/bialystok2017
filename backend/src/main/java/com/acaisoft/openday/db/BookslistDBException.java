package com.acaisoft.openday.db;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BookslistDBException extends Exception {

    public BookslistDBException(String message) {
        super(message);
    }
}
