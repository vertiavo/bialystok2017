# Frontend

## Zadanie podstawowe

#### Treść zadania

Utworzenie formularza rejestracji użytkownika.

#### Opis

Celem zadania jest utworzenie działającego formularza rejestracji użytkownika.
Formularz powinien zawierać pola: `email`, `password`, `confirmPassword`. 

Pole `email` powinno:

 * być wymagane
 
 * być poprawne tylko wtedy, gdy podany został email w prawidłowym formacie


Pola `password` i `confirmPassword` powinny:

 * być wymagane,
 
 * być ze sobą zsynchronizowane, to znaczy, że wartości tych pól powinny być identyczne, aby formularz był traktowany jako poprawny.
 
W przypadku nie spełnienia którychkolwiek założeń powinna zostać wyświetlona odpowiednia informacja.

===================================================

Po zatwierdzeniu formularza należy wysłać zapytanie metodą `POST` pod następujący adres: `/api/user/register/simple`.
Ciało zapytania wygląda następująco:
```json
{
  "email": "string",
  "password": "string"
}
```

Po uzyskaniu odpowiedzi ze statusem `200` należy wyświetlić odpowiednie powiadomienie (użyj `NotificationsService`),
oraz przekierować użytkownika na stronę `/login`.

W przypadku odpowiedzi ze statusem inny niż `200` należy wyświetlić odpowiednie powiadomienie (użyj `NotificationsService`)

POWODZENIA.

## Zadanie rozszerzone

#### Treść zadania

 1. Sprawdzenie, czy użytkownik o podanym adresie email już istnieje. 
 
 2. Rozszerzenie poprzedniego formularza o przepisywanie captchy.

#### Opis

 * Zapytanie sprawdzające należy wysłać metodą `GET` na adres: `/api/user/exists/{email}`,
 gdzie za `{email}` należy podstawić email do sprawdzenia.
 
 W odpowiedzi zostanie zwrócony następujący obiekt: 
```json
{
  "userExists": "boolean"
}
```
Jeśli użytkownik z takim emailem istnieje pole `userExists` będzie ustawione na `true`.

===================================================

 * Użytkownik powinien móc przepisać ciąg znaków, aby sprawdzić, czy nie jest robotem.
 
 Ciąg znaków (captche) należy pobrać za pomocą metody `GET` z następującego adresu: `/api/user/register/captcha/generate`.
 
 W odpowiedzi zostanie zwrócony następujący obiekt:
```json
{
  "formId": "string",
  "textValue": "string"
}
```

Pole `textValue` jest ciągiem znaków, który użytkownik musi przepisać aby poprawnie zwalidować formularz.

====================================================

Po zatwierdzeniu formularza należy wysłać zapytanie metodą `POST` pod następujący adres: `/api/user/register/captcha`.
Ciało zapytania wygląda następująco:
```json
{
  "email": "string",
  "password": "string",
  "captcha": {
    "formId": "string",
    "textValue": "string"
  }
}
```

Gdzie pole `captcha` jest wcześniej pobranym obiektem captchy.

Po uzyskaniu odpowiedzi ze statusem `200` należy wyświetlić odpowiednie powiadomienie (użyj `NotificationsService`),
oraz przekierować użytkownika na stronę `/login`.

W przypadku odpowiedzi ze statusem inny niż `200` należy wyświetlić odpowiednie powiadomienie (użyj `NotificationsService`)

POWODZENIA.

