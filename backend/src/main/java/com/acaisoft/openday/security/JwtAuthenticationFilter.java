package com.acaisoft.openday.security;


import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

public class JwtAuthenticationFilter extends OncePerRequestFilter {
    public static final String JWT_HEADER_TOKEN_NAME = "JWT_TOKEN";

    private final JwtFactory jwtFactory;
    private final AuthenticationExceptionHandler exceptionHandler;

    public JwtAuthenticationFilter(JwtFactory jwtFactory, AuthenticationExceptionHandler exceptionHandler) {
        this.jwtFactory = jwtFactory;
        this.exceptionHandler = exceptionHandler;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try{

            String jwtTokenValue = request.getHeader(JWT_HEADER_TOKEN_NAME);
            if(StringUtils.isBlank(jwtTokenValue)){
                filterChain.doFilter(request, response);
                return;
            }

            JwtUser jwtUser = jwtFactory.jwt().jwtUser(jwtTokenValue);

            JwtAuthenticationToken jwtAuthenticationToken = new JwtAuthenticationToken(jwtUser);
            SecurityContextHolder.getContext().setAuthentication(jwtAuthenticationToken);

            filterChain.doFilter(request, response);
        }catch (Exception ex){
            exceptionHandler.onAuthenticationFailure(request, response, ex);
        }

    }
}
