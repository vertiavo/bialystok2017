package com.acaisoft.openday.views.user.dto;

public class UserExistsDto {
    private boolean userExists;

    public boolean isUserExists() {
        return userExists;
    }

    public UserExistsDto setUserExists(boolean userExists) {
        this.userExists = userExists;
        return this;
    }
}
