package com.acaisoft.openday.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;

public class JwtAuthenticationToken extends AbstractAuthenticationToken {

    private static final long serialVersionUID = -2952499398501597811L;
    private JwtUser user;

    public JwtAuthenticationToken(JwtUser user) {
        super(user.getAuthorities());
        setAuthenticated(true);
        this.user = user;
    }

    @Override
    public Object getCredentials() {
        return "";
    }

    @Override
    public JwtUser getPrincipal() {
        return user;
    }

}
