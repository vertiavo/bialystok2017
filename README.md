# Installation

### Backend

Requirements:

 * Java 8
 
 * Maven
 
Inside a `/backend` directory run the following command:

```
mvn spring-boot:run
```

This will automatically install all the required dependencies
and then run the server at address `http://localhost:8080/`
 
### Frontend

Requirements:

 * Nodejs 7.8
 
Inside a `/frontend` directory run the following commands:

```
npm install
npm run servdevloc
```

The first command will automaticaly install all the required dependencies.
The second will run the server at address `http://localhost:4200`.
The server redirects all AJAX calls to the `http://localhost:8080`

