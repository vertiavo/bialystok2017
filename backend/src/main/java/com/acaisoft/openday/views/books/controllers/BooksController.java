package com.acaisoft.openday.views.books.controllers;

import com.acaisoft.openday.db.BookslistDB;
import com.acaisoft.openday.db.BookslistDBException;
import com.acaisoft.openday.db.UsersDB;
import com.acaisoft.openday.schema.Book;
import com.acaisoft.openday.schema.User;
import com.acaisoft.openday.views.books.exceptions.BookBorrowException;
import com.acaisoft.openday.views.user.dto.UserExistsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class BooksController {
    private BookslistDB bookslistDB;
    private UsersDB usersDB;

    @Autowired
    public BooksController(BookslistDB bookslistDB, UsersDB usersDB) {
        this.bookslistDB = bookslistDB;
        this.usersDB = usersDB;
    }

    @GetMapping("/bookslist")
    public ResponseEntity<List<Book>> getBooksList() {
        return ResponseEntity.ok(this.bookslistDB.getBooksList());
    }

    @GetMapping("/bookslist/{bookId}/borrow")
    public ResponseEntity borrowBook(String username, @PathVariable Integer bookId) throws BookBorrowException {
        Optional<User> userOpt = this.usersDB.getUser(username);

        UserExistsDto response = new UserExistsDto().setUserExists(userOpt.isPresent());
        if (response.isUserExists())
            return ResponseEntity.ok(this.bookslistDB.borrowBook(bookId));
        else
            throw new BookBorrowException("No such user in database");
    }
}
