package com.acaisoft.openday.db;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UsersDBException extends Exception{

    public UsersDBException(String message) {
        super(message);
    }
}
