package com.acaisoft.openday.security;


public interface JwtFactory {

    Jwt jwt();
}
