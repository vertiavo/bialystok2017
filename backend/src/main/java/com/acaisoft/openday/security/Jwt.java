package com.acaisoft.openday.security;


import org.springframework.security.core.userdetails.UserDetails;

public interface Jwt {

    JwtUser jwtUser(String text) throws Exception;

    String token(UserDetails userDetails) throws Exception;
}
