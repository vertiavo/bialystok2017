package com.acaisoft.openday.security.impl;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class UserDetailsWrap implements UserDetails{
    private final UserDetails origin;

    public UserDetailsWrap(UserDetails origin) {
        this.origin = origin;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.origin.getAuthorities();
    }

    @Override
    public String getPassword() {
        return this.origin.getPassword();
    }

    @Override
    public String getUsername() {
        return this.origin.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.origin.isAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.origin.isAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.origin.isCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return this.origin.isEnabled();
    }
}
